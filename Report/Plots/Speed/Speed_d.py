# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('Experiments/SpeedTest.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

d = [int(x[1]) for x in data if x[0]== 'FastFood input d' ]
fastfood = [float(x[5])*(10**-6) for x in data if x[0] == 'FastFood input d']
TS = [float(x[5])*(10**-6) for x in data if x[0] == 'TS input d']


fig, ax = plt.subplots()
ax.plot(d, fastfood, 'b^', label='FastFood transform')
ax.plot(d, fastfood, 'k--', label='_nolegend_')
ax.plot(d, TS, 'ro', label='TensorSketch transform')
ax.plot(d, TS, 'k--', label='_nolegend_')

ax.set_xlim(2, 4096)
ax.set_xticks([2,256,512,1024,2048,4096])

#ax.set_yscale('log',basey=2)
#ax.set_xscale('log',basex=2)

#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=2)
plt.xlabel('Input dimensions,d')
plt.ylabel('Seconds on average')

plt.savefig('Speed_d.png', bbox_inches=0,dpi=300)

