# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('TSsparse50.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

axislabels = [float(x[0])*100 for x in data]
TS = [float(x[1]) for x in data]



fig, ax = plt.subplots()
ax.plot(axislabels, TS, 'b^', label='TensorSketching d=50,D=500,p=2,C=0')
ax.plot(axislabels, TS, 'k--', label='_nolegend_')


ax.set_xlim(0, 100)
ax.set_xticks([0,12.5,25,37.5,50,62.5,75,87.5,97.5])


#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=1)
plt.xlabel('Sparseness. % empty entries in d')
plt.ylabel('Average relative error')

plt.savefig('TSsparseError.png', bbox_inches=0,dpi=300)

