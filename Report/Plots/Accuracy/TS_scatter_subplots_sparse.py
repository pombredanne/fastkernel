# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open('Experiments/0TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel0 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast0 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]


with open('Experiments/10TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:

    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel1 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast1 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/20TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel2 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast2 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/30TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel3 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast3 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/40TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel4 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast4 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/50TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel5 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast5 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/60TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel6 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast6 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/70TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel7 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast7 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/80TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel8 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast8 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

f, axarr = plt.subplots(3,3)
axarr[0, 0].scatter(kernel0,fast0,s=2,alpha=0.1)
axarr[0, 1].scatter(kernel1,fast1,s=2,alpha=0.1)
axarr[0, 2].scatter(kernel2,fast2,s=2,alpha=0.1)
axarr[1, 0].scatter(kernel3,fast3,s=2,alpha=0.1)
axarr[1, 1].scatter(kernel4,fast4,s=2,alpha=0.1)
axarr[1, 2].scatter(kernel5,fast5,s=2,alpha=0.1)
axarr[2, 0].scatter(kernel6,fast6,s=2,alpha=0.1)
axarr[2, 1].scatter(kernel7,fast7,s=2,alpha=0.1)
axarr[2, 2].scatter(kernel8,fast8,s=2,alpha=0.1)

plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.setp([a.get_xticklabels() for a in axarr[1, :]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 1]], visible=False)
plt.setp([a.get_yticklabels() for a in axarr[:, 2]], visible=False)

plt.setp([a for a in axarr],ylim=(0,100))
plt.setp([a for a in axarr],xlim=(0,100))

plt.savefig('TS_Scatter_sparse_subplots'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.png', bbox_inches=0,dpi=300)

