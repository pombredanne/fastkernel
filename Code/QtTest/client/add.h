#ifndef ADD_H
#define ADD_H
#include <shark/LinAlg/Base.h>

class Add
{
public:
    Add(double offset) : m_offset(offset){}

    typedef shark::RealVector result_type;

    shark::RealVector operator()(shark::RealVector input) const {
        for (std::size_t i =0; i != input.size(); ++i)
            input(i) += m_offset;
        return input;
    }
private:
    double m_offset;
};

#endif // ADD_H
