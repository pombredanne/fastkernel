#ifndef LAB_H
#define LAB_H
#include <string>
#include <stacker.h>
#include <kernels.h>
#include <TensorSketch.h>
#include <shark/Models/Kernels/PolynomialKernel.h>
#include <resizer.h>


#include "experiment.h"


class Lab
{
public:
    Lab();
    int SpeedTest(string filename, unsigned int d_, unsigned int n_);
    int AccuracyTest_FF_Gauss(unsigned i, unsigned d, unsigned D, double sigma,int n,unsigned int pad=0);
    int AccuracyTest_TS_Poly(unsigned i,unsigned d, unsigned D,int p, double C,int n,double sparse=0);
    int InnerProdTest();
    int DifferenceTest();
};

#endif // LAB_H
