#ifndef STACKER_H
#define STACKER_H
#include <vector>
#include <fastfood.h>
#include <dataappender.h>
#include <fastkernelexceptions.h>
#include <Common.h>
#include <shark/LinAlg/Base.h>

///
/// \brief Stacker::Stacker
///The FastFood class can provide V with D=d for a 2^i input.
///The stacker is used when D>d.
///The FastFood class should only be used seperatly for testing purposes, stacker class provides a more robust interface for production.
///D is the number of basis functions used. The name is chosen to reference Dimensionality of the featurespace
///d is the dimensionality of the input space. Use dataappend transform to make the dataset dimensionality 2^i by appending or cropping.
class FastFoodStacked
{
public:
    void print_operators();
    FastFoodStacked(unsigned int d_,unsigned int D_,double sigma_=1);
    FastFoodStacked(vector<FastFood> V_);
    shark::RealVector map(const shark::RealVector &x) const;
    shark::RealVector Vx(const shark::RealVector &x) const;
    shark::RealVector normalize_exp(shark::RealVector& input) const;
    shark::RealVector normalize_2cos_exp(shark::RealVector& input) const;

    typedef shark::RealVector result_type;
    shark::RealVector operator() (shark::RealVector input) const{
        input = this->map(input);
        return input;
    }

    unsigned int features();
private:
    std::vector<FastFood> m_V;
    unsigned int m_D;
    unsigned int m_d;
    unsigned int m_D_div_d;
    double m_normalization_const;
    double m_b;
};

#endif // STACKER_H
