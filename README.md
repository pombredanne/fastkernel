# Fastfood and tensor sketching with C++

A C++ implementation of fastfood and tensor sketching for standalone transformations or incorporation in [Shark](http://image.diku.dk/shark/).


## What is fastfood and tensor sketching?

Both are random mapping techniques useful in machine learning. They combine benefits from kernel methods with linear time classification and regression. Both techniques are used and  described in my thesis: [Scalable learning through linearithmic time kernel approximation techniques](https://bytebucket.org/johanvts/fastkernel/raw/2ab064ebfc8e3e37e3d90536fd13efeb86ac9a39/Report/FastKernel/FastKernel.pdf). For the original papers see [Fastfood](http://ai.stanford.edu/~quocle/LeSarlosSmola\_ICML13\_supp.pdf), [Tensor sketching](http://www.itu.dk/people/pagh/papers/tensorsketch.pdf). 

## How do I test it?

To recreate experiments from the thesis you can use the "client" binary in 'Report/Plots/Accuracy/'. This provides some methods for testing both Fastfood and tensor sketch. To get help execute the binary with no parameters and a help text will be displayed. You can also build your own by navigating to the 'QtTest/client' and configure a build using cmake. You need an installation of Shark [Shark](http://image.diku.dk/shark/) and [FFTW++](http://fftwpp.sourceforge.net/) to build.

```
$ ccmake .
```

to configure your build.

## How do I use it?

The main implementation is found in 'Code/fastfood' and 'Code/tensorsketch'. Incorporate these files in your own projects. This is easiest when you use the shark library. From here you can consider both libraries [Data transforms](http://image.diku.dk/shark/sphinx_pages/build/html/rest_sources/tutorials/concepts/data/datasets.html#querying-information-about-a-dataset). In any case you need to construct an object of either type. Parameters are given on construction. You can subsequently map data by calling the 'map(vector x)' method.  

